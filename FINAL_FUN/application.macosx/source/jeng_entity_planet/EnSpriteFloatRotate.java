package jeng_entity_planet;

import jeng_entity_sprite.EnSpriteStatic;
import jeng_game.MainGame;
import jeng_utilities.PairFloat;
import jeng_utilities.Rectangle;
import processing.core.PApplet;
import processing.core.PImage;

public class EnSpriteFloatRotate extends EnSpriteStatic {

	float drawRotate = 0;
	public float drawRotateVel = 0;
	
	public PairFloat velVector;
	
	//quick dirty thing for collision detection of container
	public boolean hasCollide = false;
	
	public EnSpriteFloatRotate(MainGame ap, String filename, float adrawRotateVel, float velX, float velY, float posX, float posY )
	{
		super(ap, filename);
		velVector = new PairFloat(velX,velY);
		pos = new PairFloat(posX,posY);
		drawRotateVel = adrawRotateVel;
		
		mySg.advanceFrames( (int)p.random(9) );
		
	}
	
	public String toString() { return "EnSpriteFloatRotate"; }
	
	public void update()
	{
		//float
		pos.x += velVector.x*time.getSecPast();
		pos.y += velVector.y*time.getSecPast();
		
		//rotate
		drawRotate += drawRotateVel*time.getSecPast();
		
		time.update();
	}
	public void draw()
	{
		//grab the image
		myFrame = mySg.grabFrame(myState);
		PImage tempImage = myFrame.myImage.get
			(						
				myFrame.myDrawBox.x, 
				myFrame.myDrawBox.y, 
				myFrame.myDrawBox.w, 
				myFrame.myDrawBox.h						
			);
	
		p.pushMatrix();
		p.fill(255);
		
		p.rotateAt(drawRotate/180,pos.x,pos.y);
		p.imageMode(PApplet.CENTER);
		p.image( tempImage, pos.x, pos.y );
		p.imageMode(PApplet.CORNERS);
		p.noFill();
		p.popMatrix();
	}
	
	public Rectangle getHitRect()
	{
		hitRect.x = pos.x + myFrame.myHitBox.get(0).x;
		hitRect.y = pos.y + myFrame.myHitBox.get(0).y;
		hitRect.w = myFrame.myHitBox.get(0).w;
		hitRect.h = myFrame.myHitBox.get(0).h;
		return hitRect;
	}
}
