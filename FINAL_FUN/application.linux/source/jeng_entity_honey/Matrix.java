package jeng_entity_honey;


import java.util.List;
import java.util.Vector;

public class Matrix<T> 
{
	List<T> ls;
	int nWidth, nLength;
	
	public Matrix(int nWidth_, int nLength_)
	{
		nWidth = nWidth_;
		nLength = nLength_;
		ls = new Vector<T>(nWidth*nLength);
		
		/*
		for(int i = 0; i < nWidth*nLength; i++)
		{
			ls.add(null);
		}
		*/
	}
	
	public T at(int x, int y)
	{
		if( x < 0 || x >= nWidth || y < 0 || y >= nLength)
			return null;
		
		/*
		PApplet.println(ls.size());
		PApplet.print(x);
		PApplet.print(" ");
		PApplet.println(y);
		*/
		
		return ls.get(y*nWidth + x);
	}
	
	public void set(int x, int y, T t)
	{
		if( x < 0 || x >= nWidth || y < 0 || y >= nLength)
			return;
		
		ls.set(y*nWidth + x,t);
	}
	
	public int size()
	{
		return nWidth*nLength;
	}
	
	public boolean isSet()
	{
		return	ls.size() == nWidth*nLength;
	}
}
