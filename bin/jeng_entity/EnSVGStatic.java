package jeng_entity;

import jeng_game.MainGame;
import processing.core.*;

public class EnSVGStatic extends EnHit {

	PShape myShape;
	float zoomRatio = 1;
	public EnSVGStatic(MainGame ap, String filename)
	{
		super(ap);
		myShape = p.loadShape(filename);
	}
	
	public EnSVGStatic(MainGame ap, String filename, float azoomRatio)
	{
		super(ap);
		myShape = p.loadShape(filename);
		zoomRatio = azoomRatio;
	}
	
	public void draw()
	{
		if(zoomRatio == 1)
			p.shape(myShape,pos.x, pos.y, myShape.width, myShape.height);
		else
		{
			p.myCam.reverseTransform();
			p.myCam.feed(zoomRatio);
			p.smooth();
			
			p.shapeMode(PApplet.CENTER);
			p.shape(myShape,pos.x, pos.y, myShape.width, myShape.height);
			p.shapeMode(PApplet.CORNERS);
			
			p.noSmooth();
			p.myCam.reverseTransform();
			p.myCam.feed();
		}
	}
	
	//get hit box
	//should return PShape hitbox
}
