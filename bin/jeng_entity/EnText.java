package jeng_entity;

import processing.core.*;
import jeng_game.MainGame;
import jeng_utilities.PairFloat;
import jeng_utilities.Rectangle;

public class EnText extends Entity{
	
	public PairFloat pos = new PairFloat(500,500);
	public String m_text;
	public String m_text2;
	
	PFont myFont;
	
	public EnText(MainGame ap)
	{
		super(ap);
		myFont = p.loadFont("font2.ttf");
	}	
	
	public void update()
	{
		m_text = Float.toString((int)p.myCam.getMousePos(p.mouseX, p.mouseY).x);
		m_text2 = Float.toString((int)p.myCam.getMousePos(p.mouseX, p.mouseY).y);
	}
	
	public void draw()
	{
		p.textFont(myFont, 12);
		p.stroke(0);
		p.text(m_text, pos.x, pos.y);
		p.text(m_text2, pos.x+20, pos.y);
		
	}
}
