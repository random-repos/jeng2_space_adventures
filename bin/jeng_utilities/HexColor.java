package jeng_utilities;

import processing.core.PApplet;

public class HexColor {
	//for now
	public int r = 0;
	public int g = 0;
	public int b = 0;
	
	public HexColor() {};
	public HexColor(int _r, int _g, int _b)
	{
		r = _r;
		g = _g;
		b = _b;
	}
	
	
	//specificall for planets
	//temporary stuff
	int top = 180;
	int bottom = 240;
	public void setRandom_light(PApplet ap)
	{
		r = (int)ap.random(top,bottom);
		g = (int)ap.random(top,bottom);
		b = (int)ap.random(top,bottom);
		
	}
}
